{
  description = "iamy";

  inputs = {
    #nixpkgs.url = "git+file:///Users/viraptor/src/nixpkgs?rev=d9479abed960f250eb3fa012b2ab8df47a4274cf";
    nixpkgs.url = "github:NixOS/nixpkgs/staging";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    let with-system = flake-utils.lib.eachDefaultSystem
      (system:
        let
          pkgs = import nixpkgs { inherit system; };
        in rec {
          packages.radicle-cli = pkgs.rustPlatform.buildRustPackage rec {
            pname = "radicle-cli";
            version = "0.3.1";
            nativeBuildInputs = [ pkgs.cmake ];
            buildInputs = [ pkgs.openssl.dev pkgs.darwin.apple_sdk.frameworks.IOKit pkgs.darwin.apple_sdk.frameworks.AppKit ];
            src = pkgs.fetchFromGitHub {
              owner = "radicle-dev";
              repo = pname;
              rev = "v${version}";
              sha256 = "";
            };
            cargoSha256 = "";
          };

          packages.iamy = pkgs.buildGoModule rec {
            pname = "iamy";
            version = "4.0.0";
          
            src = pkgs.fetchFromGitHub {
              owner = "envato";
              repo = pname;
              rev = "v${version}";
              sha256 = "sha256-VvveRJogeNnwvYtyqa7f8n2s4FLQoZJiLMJo5Przp18=";
            };
          
            vendorSha256 = "sha256-joVNR5pqkYNd9HQ4retqvj1RfdkluSdsfrSVnMtLans=";

            proxyVendor = true;
            
            doCheck = false;
          
            ldflags = [
              "-X main.Version=v${version}+envato" "-s" "-w"
            ];
          
            meta = with pkgs.lib; {
              description = "A cli tool for importing and exporting AWS IAM configuration to YAML files";
              homepage = "https://github.com/envato/iamy";
              license = licenses.mit;
            };
          };
          
          packages.ejson2env = pkgs.buildGoModule rec {
            pname = "ejson2env";
            version = "2.0.2";
          
            src = pkgs.fetchFromGitHub {
              owner = "Shopify";
              repo = pname;
              rev = "v${version}";
              sha256 = "sha256-1nfMmjYKRo5vjOwLb3fX9SQ0CDHme1DAz0AGGpV4piI=";
            };
          
            vendorSha256 = "sha256-v44iJv+qU3Nd8WwwOdBN5p+ea5o+fA0v5qzKQcBvwkM=";

            proxyVendor = true;
            
            meta = with pkgs.lib; {
              description = "ejson2env is a tool to simplify storing secrets that should be accessible in the shell environment in your git repo. ";
              homepage = "https://github.com/Shopify/ejson2env";
              license = licenses.mit;
            };
          };

          packages.rbspy = pkgs.rustPlatform.buildRustPackage rec {
            pname = "rbspy";
            version = "0.11.1";
            src = pkgs.fetchFromGitHub {
              owner = pname;
              repo = pname;
              rev = "v${version}";
              sha256 = "sha256-9BeQHwwnirK5Wquj6Tal8yCU/NXZGaPjXZe3cy5m98s=";
            };
            cargoSha256 = "sha256-DHdfv6210wAkL9vXxLr76ejFWU/eV/q3lmgsYa5Rn54=";
            doCheck = true;
          };

          packages.cf-vault = pkgs.buildGoModule rec {
            pname = "cf-vault";
            version = "0.0.11";

            src = pkgs.fetchFromGitHub {
              owner = "jacobbednarz";
              repo = pname;
              rev = version;
              sha256 = "sha256-Imd9qeT4xg5ujVPLHSSqoteSPl9t97q3Oc4C/vzHphg=";
            };

            proxyVendor = true;

            vendorSha256 = "sha256-SEC/qhvBmUpdXvAHWKylyiBl/MxThFAqy+0DcTDb9N4=";
          };

          packages.yubico-piv-tool = pkgs.stdenv.mkDerivation rec {
            pname = "yubico-piv-tool";
            version = "2.2.1";
          
            src = pkgs.fetchurl {
              url = "https://developers.yubico.com/yubico-piv-tool/Releases/yubico-piv-tool-${version}.tar.gz";
              sha256 = "sha256-t+3k3cPW4x3mey4t3NMZsitAzC4Jc7mGbQUqdUSTsU4=";
            };
          
            nativeBuildInputs = [ pkgs.pkg-config pkgs.cmake pkgs.gengetopt ];
            buildInputs = [ pkgs.openssl pkgs.check ]
              ++ (if pkgs.stdenv.isDarwin then [ pkgs.darwin.apple_sdk.frameworks.PCSC ] else [ pkgs.pcsclite ]);
          
            cmakeFlags = [
              "-DGENERATE_MAN_PAGES=OFF" # TODO: help2man tries to run the tool from the build directory which can't load the shared library
              "-DCMAKE_INSTALL_BINDIR=bin"
              "-DCMAKE_INSTALL_INCLUDEDIR=include"
              "-DCMAKE_INSTALL_MANDIR=share/man"
              "-DCMAKE_INSTALL_LIBDIR=lib"
            ];
          
            configureFlags = [ "--with-backend=${if pkgs.stdenv.isDarwin then "macscard" else "pcsc"}" ];
          
            meta = with pkgs.lib; {
              homepage = "https://developers.yubico.com/yubico-piv-tool/";
              description = ''
                Used for interacting with the Privilege and Identification Card (PIV)
                application on a YubiKey
              '';
              longDescription = ''
                The Yubico PIV tool is used for interacting with the Privilege and
                Identification Card (PIV) application on a YubiKey.
                With it you may generate keys on the device, importing keys and
                certificates, and create certificate requests, and other operations.
                A shared library and a command-line tool is included.
              '';
              license = licenses.bsd2;
              platforms = platforms.all;
            };
          };

          apps = {
            cf-vault = {
              type = "app";
              program = "${packages.cf-vault.${system}}/bin/cf-vault";
            };
            rbspy = {
              type = "app";
              program = "${packages.rbspy.${system}}/bin/rbspy";
            };
            iamy = {
              type = "app";
              program = "${packages.iamy.${system}}/bin/iamy";
            };
            yubico-piv-tool = {
              type = "app";
              program = "${packages.yubico-piv-tool.${system}}/bin/yubico-piv-tool";
            };
          };
        }
      );

    in {
      packages = with-system.packages;
      apps = with-system.apps;
      overlay = final: prev: {
        rbspy = with-system.packages.${prev.system}.rbspy;
        iamy = with-system.packages.${prev.system}.iamy;
        cf-vault = with-system.packages.${prev.system}.cf-vault;
        yubico-piv-tool = with-system.packages.${prev.system}.yubico-piv-tool;
        ejson2env = with-system.packages.${prev.system}.ejson2env;
        defaultGemConfig = prev.defaultGemConfig // {
          exiv2 = attrs: {
            buildFlags = [ "--with-exiv2-lib=${prev.exiv2}/lib" "--with-exiv2-include=${prev.exiv2.dev}/include" ];
          };
          maxmind_geoip2 = attrs: {
            buildFlags = [ "--with-maxminddb-lib=${prev.libmaxminddb}/lib" "--with-maxminddb-include=${prev.libmaxminddb}/include" ];
          };
        };
      };
    };
}
